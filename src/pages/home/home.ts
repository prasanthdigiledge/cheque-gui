import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AddTransactionPage } from '../add-transaction/add-transaction';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  choose: string = "Sent";
  products : any = [];
  received : any = [];
  constructor(public navCtrl: NavController,
              public http : Http) {
    let Data = this.http.get('../../assets/data.json').map(res => res.json().items);
    Data.subscribe(
      data => { this.products = data;
      })
    let ReceivedData = this.http.get('../../assets/receiveddata.json').map(res => res.json().receiveditems);
    ReceivedData.subscribe(
      data => {
        this.received = data;
      }
    )
  }
  expend(i)
  {
    // console.log(i);
    this.products[i].open = !this.products[i].open;

    for(let key in this.products)
    {
       if(this.products[key].open != undefined && key != i)
       {
         this.products[key].open = false;
       }
    }
  }

  received_expend(i)
  {
    this.received[i].open = !this.received[i].open;
    for(let key in this.received)
    {
      if(this.received[key].open != undefined && key != i)
      {
        this.received[key].open = false;
      }
    }
  }

  addCheque(){
    this.navCtrl.push(AddTransactionPage);
  }

}
