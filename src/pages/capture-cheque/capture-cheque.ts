import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Camera, CameraOptions } from '@ionic-native/camera';
/**
 * Generated class for the CaptureChequePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-capture-cheque',
  templateUrl: 'capture-cheque.html',
})
export class CaptureChequePage {
  image : string;
  options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private camera : Camera
            ) {
  }

  

  ionViewDidLoad() {
    console.log('ionViewDidLoad CaptureChequePage');
  }
  async submit(): Promise<any>{
    try{
      this.image = await this.camera.getPicture(this.options);
    }
    catch(err)
    {
      console.log(err);
    }
    // this.navCtrl.push(HomePage);
  }
}
