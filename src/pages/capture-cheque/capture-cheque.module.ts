import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CaptureChequePage } from './capture-cheque';

@NgModule({
  declarations: [
    CaptureChequePage,
  ],
  imports: [
    IonicPageModule.forChild(CaptureChequePage),
  ],
})
export class CaptureChequePageModule {}
