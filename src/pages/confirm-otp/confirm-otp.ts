import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { WelcomepagePage } from '../welcomepage/welcomepage';

/**
 * Generated class for the ConfirmOtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm-otp',
  templateUrl: 'confirm-otp.html',
})
export class ConfirmOtpPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public loadingCtrl : LoadingController) {
  }
  next(el) {
    el.setFocus();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmOtpPage');
  }
  confirm(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();

    setTimeout(() => {
      loading.dismiss();
    }, 1000);
    this.navCtrl.push(WelcomepagePage);
  }

}
