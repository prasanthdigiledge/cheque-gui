import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmOtpPage } from './confirm-otp';

@NgModule({
  declarations: [
    ConfirmOtpPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmOtpPage),
  ],
})
export class ConfirmOtpPageModule {}
