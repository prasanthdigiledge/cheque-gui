import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CaptureChequePage } from '../capture-cheque/capture-cheque';

/**
 * Generated class for the AddTransactionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-transaction',
  templateUrl: 'add-transaction.html',
})
export class AddTransactionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  submit(){
    this.navCtrl.push(CaptureChequePage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddTransactionPage');
  }

}
