#Components
   add-transaction component
   capture-cheque component
   confirm-otp component
   home component
   login component
   welcomepagw component
   list component

#Module
   1.app
   2.pages

#Components Structure
#app - app component
       app.component.ts        
       app.html                 -- app selector and side bar
       app.module.ts            -- It include all the component
       app.scss
       main.ts

       Description:
           It is the main module and it has sidebar
#pages
   1.login component
       login.html               -- login form
       login.module.ts
       login.scss
       login.ts                 -- login conditons
  
       Description:
           It will sent otp and navigate to otp component
           
   2.home component
       home.html                
       home.scss
       home.ts                

       Description:

          It will have two tabs sent and recived cheque 
      
     3.Confirm-Otp Component

              confirm-otp.html
              confirm-otp.module.ts
              confirm-otp.scss
              confirm-otp.ts

              Description
                it verify the ur otp

       4. capture-cheque component

               capture-cheque.html
               capture-cheque.module.ts
               capture-cheque.scss
               capture-cheque.ts

               Description
                This will capture cheque and sent it to microsoft azure and give back json file

       5.welcomepage Component
               welcomepage.html
               welcomepage.module.ts
               welcomepage.scss
               welcomepage.ts
      
       Description

           after login this page will get load
    

       6.add-transaction Component
               add-transaction.html
               add-transaction.module.ts
               add-transaction.scss
               add-transaction.ts

       Description
           after capture cheque this page will get load

