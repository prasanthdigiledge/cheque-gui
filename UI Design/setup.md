#Cheque Mobile App
#Environment Setup
#Install Ionic CLI and Android Studio
#Ionic CLI                         
  npm install -g ionic

#Android Studio   
  https://developer.android.com/studio/

#Android SDK
  sudo apt-get install android-sdk

#Setup Environment Variable

    sudo gedit ~/.bashrc

    export ANDROID_HOME=/home/user_directory/Android/Sdk

    export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools

    export JAVA_HOME=/usr/lib/jvm/java-8-oracle

#For ref :  https://medium.com/@aashimad1/install-android-studio-in-ubuntu-b8aed675849f 

#Bitbucket Repository

Repository Name  : cheque-gui
https://bitbucket.org/prasanthdigiledge/cheque-gui/src/master/
Branch : Master

#Dependencies 
No additional dependencies.just try to install npm package.
    Sudo npm install 
#Build Setup
For running lockaly 
    Sudo ionic serve 
#Platform
#Android Platform 
     ionic cordova platform add android
#IOS Platform
   ionic cordova platform add ios
#Run in mobile application
#Android Platform 
      ionic cordova build android
      ionic cordova run android
#IOS Platform
     ionic cordova build ios
    ionic cordova run ios


#Design Documents
    Click the below link to find the design document.
    https://drive.google.com/open?id=1ENiksqlt44FJxBN2z4dFwOVuT8skbLFJIOJwg3EACMQ
